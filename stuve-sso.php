<?php

/*
Plugin Name:      Stuve SSO
Plugin URI:       https://gitlab.rrze.fau.de/stuve/referat-it/wordpress/stuve-sso
Description:      Stuve-Ergänzungen zu rrze-sso.
Version:          1.0.0
Author:           Tom Kunze
License:          GNU General Public License v2
License URI:      http://www.gnu.org/licenses/gpl-2.0.html
Text Domain:      stuve-sso
Network:          true
*/

namespace Stuve\SSO;

defined('ABSPATH') || exit;

add_action('edit_user_profile', __NAMESPACE__ . '\disable_user_fields');
add_action('show_user_profile', __NAMESPACE__ . '\disable_user_fields');
add_action('user_profile_update_errors', __NAMESPACE__ . '\prevent_user_changes', 10, 3);

function disable_user_fields() {
	?>
	<script type="text/javascript">
        jQuery(function() {
            // Hide input field and show value in cloned disabled input
            // Hide real inputs only as some fields are required to be sent
            const disabled_inputs = ['first_name', 'last_name', 'nickname', 'display_name', 'email'];
            for (disabled_input of disabled_inputs) {
                var field = jQuery('#' + disabled_input);
                field.parent().append('<input type="text" value="' + field.val() + '" class="regular-text" disabled=""> <span class="description">Daten aus dem Single Sign-On können hier nicht geändert werden.</span>');
                field.hide();
            }

            jQuery('.user-nickname-wrap').hide();
            jQuery('#email-description').hide();
        });
   	</script>
	<?php
}

function prevent_user_changes($errors, $update, $user) {
    $old_user = get_user_by('id', $user->ID);

    // Mapping of disabled html inputs to old user values
    $disabled_inputs = [
        'first_name' => get_user_meta($user->ID, 'first_name', true),
        'last_name' => get_user_meta($user->ID, 'last_name', true),
        'nickname' => get_user_meta($user->ID, 'nickname', true),
        'display_name' => $old_user->display_name,
        'email' => $old_user->user_email,
    ];
    foreach ($disabled_inputs as $disabled_input => $old_value) {
        if (isset($_POST[$disabled_input]) && $_POST[$disabled_input] != $old_value) {
            $errors->add('stuve_sso_error', 'Daten aus dem Single Sign-On können hier nicht geändert werden. (' . $disabled_input . ')');
        }
    }

}
